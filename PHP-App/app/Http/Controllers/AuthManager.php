<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Auth; // Make sure to include the Auth facade at the top of your file

class AuthManager extends Controller
{
    function login() {
        return view('login');
    }

    function register() {
    return view('register');
    }


    public function loginPost(Request $request) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
    
        $credentials = $request->only('email', 'password');
    
        if (Auth::attempt($credentials)) {
            return redirect()->intended(route('home'));
        }
    
        return redirect(route('login'))->with('error', 'Invalid login credentials');
    }
    
    
    function registerPost(Request $request) {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);
        if(!$user) {
            return redirect(route('register'))->with("error", "signup failed");  
        }
        return redirect(route('login'))->with("success", "Registration success, Login to your account");
    }

    function logout() {
        Session::flush();
        Auth::logiut();
        return redirect(route('login'));
    }
}
